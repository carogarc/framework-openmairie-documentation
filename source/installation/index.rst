.. _installation:

#########################
Prérequis et installation
#########################


.. contents::


**********
Pré-requis
**********

Vous devez avoir installé :

- un serveur web (apache2, ...)
- PHP (Versions testées : 5.6 & 7.0) avec le module phpxx-mbstring
- le moteur de base de données PostGreSQL (Versions testées : 9.1 > 9.6) avec
  l'extension PostGIS (Versions testées : 2.0 > 2.3)
- la librairie XML : libxml > 2.9.0

Sous Windows, il est facile de trouver de la documentation pour l'installation
de ces éléments en utilisant wamp (http://www.wampserver.com/) par exemple.

Sous Linux, il est facile de trouver de la documentation pour l'installation de
ces éléments sur votre distribution.


************
Installation
************

Installation des fichiers du framework
======================================

Télécharger l'archive zip
-------------------------

http://adullact.net/frs/?group_id=265


Décompresser l'archive zip dans le répertoire de votre serveur web
------------------------------------------------------------------

- Exemple sous Windows dans wamp : ``C:\wamp\www\framework-openmairie``
- Exemple sous Linux avec debian : ``/var/www/html/framework-openmairie``


Création et initialisation de la base de données
================================================

Créer la base de données
------------------------

Il faut créer la base de données dans l'encodage UTF8. Par défaut la base de données s'appelle openexemple.


Dans un environnement debian :

.. code-block:: bash

    createdb openexemple


Initialiser la base de données
------------------------------

Il faut initialiser les tables, les séquences et données de paramétrage grâce 
au script :file:`data/pgsql/install.sql`.


Dans un environnement debian depuis le répertoire :file:`data/pgsql/` :

.. code-block:: bash

    psql openexemple -f install.sql


Configuration de l'applicatif
=============================

.. _creation_repertoire:

Création des répertoires dédiés au stockage des données de l'applicatif
-----------------------------------------------------------------------

A partir de la version 4.9, il faut créer les répertoires suivants :

- gen/dyn, gen/obj, gen/sql/pgsql

- obj

- sql/pgsql

- dyn

Il est également nécessaire de créer les répertoires permettant de gérer le stockage des fichiers de logs, des fichiers temporaires et des fichiers stockés. L'arborescence à créer est décrite ci-après : :ref:`arborescence_repertoire_var`.

**Remarque** : les chemins utilisés sont tous des chemins absolus.


Positionner les permissions nécessaires au serveur web
------------------------------------------------------

Dans un environnement debian : 

.. code-block:: bash

    chown -R www-data:www-data /var/www/html/framework-openmairie


Configuration de la connexion à la base de données
--------------------------------------------------

La configuration se fait dans le fichier :file:`dyn/database.inc.php` (à créer depuis la version 4.9):

.. code-block:: php

    <?php
    ...
    // PostGreSQL
    $conn[1] = array(
        "Framework openMairie", // Titre 
        "pgsql", // Type de base
        "pgsql", // Type de base
        "postgres", // Login
        "postgres", // Mot de passe
        "tcp", // Protocole de connexion 
        "localhost", // Nom d'hote
        "5432", // Port du serveur de la base de données
        "", // Socket
        "openexemple", // nom de la base
        "AAAA-MM-JJ", // Format de la date
        "framework_openmairie", // Nom du schéma
        "", // Préfixe
        null, // Paramétrage pour l'annuaire LDAP
        null, // Paramétrage pour le serveur de mail
        null, // Paramétrage pour le stockage des fichiers
    );
    ...
    ?>

Voir le paragraphe ':ref:`parametrage_connexion_base_de_donnees`' pour plus de
détails sur cette configuration.


**********************
Connexion au framework
**********************

Ouverture dans le navigateur
============================

http://localhost/framework-openmairie/

'localhost' peut être remplacé par l'ip ou le nom de domaine du serveur.


Login
=====

* Utilisateur "administrateur" : 
   - identifiant : admin
   - mot de passe : admin

Le message de bienvenue doit être affiché "Votre session est maintenant ouverte."


***************
En cas d'erreur
***************

Activer le mode debug
=====================

Il est possible d'activer le mode debug pour visualiser les messages d'erreur
détaillés directement dans l'interface. Créer le fichier :file:`dyn/debug.inc.php`,
(à créer s'il n'existe pas), il faut définir le niveau **DEBUG** à ``1``.

.. code-block:: php

    <?php
    define('DEBUG', 1);
    ?>

Voir le paragraphe ':ref:`configuration_debug_inc`' pour plus de détails sur
cette configuration.

