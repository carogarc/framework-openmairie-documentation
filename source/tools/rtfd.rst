.. _readthedocs.org:

###############
readthedocs.org
###############

readthedocs.org est un site qui héberge de la documentation, la rendant
accessible et facile à trouver. Il est possible d'importer les
documentations sur ce site depuis les système de gestion de version tel
que Subversion, Git ou d'autres. Ce site permet de gérer la mise à jour
automatique des documentations à chaque commit dans ces systèmes de gestion
de version. Le site supporte également la gestion des versions mais seulement
pour Git et non pas pour Subversion à l'heure où cette documentation est
rédigée.

L'objectif d'utiliser ce site est donc de ne pas avoir à se soucier de la 
génération des documentations. C'est ReadTheDcs.org qui s'en occupe et 
dans tous les formats html, pdf, epub, ... 

Pour pouvoir gérer un projet sur ce site, il faut avoir un utilisateur
(le bouton "Inscription" en haut à droite de la page d'acucueil permet 
d'obtenir un compte utilisateur très facilement).


==================================
Importer un nouveau projet sur RTD
==================================

Public(s) concerné(s) : Administrateur de projet openMairie.

Depuis le tableau de bord de readthedocs.org, un clic sur le bouton 
*Importer un projet*, permet d'accéder au formulaire de création d'un projet 
sphinx existant: soit votre projet se trouve dans la liste, soit vous cliquez sur *Importer manuellement*.

Voici les informations à saisir : 

* Nom : le nom du logiciel sans accents sans espaces en minuscules (par exemple : 
  openelec ou openresultat).

* Repo : l'URL du dépôt où se trouve le code source de la documentation (par 
  exemple: https://gitlab.com/openmairie/openelec-documentation.git pour openelec
  ou https://gitlab.com/openmairie/openresultat-documentation.git pour openresultat).

* Type de dépôt : "Git" ou "Subversion" en fonction du dépôt où se trouve le code source de la documentation.

* Cocher la case *Modifier les options avancées du projet* et cliquer sur *Suivant*:

  * Description : Le nom du logiciel avec accents avec espaces et avec la casse, par exemple : openElec ou openRésultat).
  
  * Type de documentation: Sphinx Html

  * Langue : "French" puisque la documentation est francophone.
  
  * Langage de programmation : Python

  * Page d'accueil du projet : "http://www.openmairie.org/".

  * Canonical URL : laissons vide pour le moment.

  * Version unique : ne pas cocher la case.

  * Etiquettes : "openmairie".

Puis il suffit de cliquer sur le bouton "Créer". 

Si la création du projet s'est bien passée une version de la documentation a 
du être générée, celle-ci est disponible en cliquant sur le bouton 
"Afficher les docs" sur la page du projet nouvellement créé.


====================================================
Paramétrer une nouvelle version d'un projet existant
====================================================

Public(s) concerné(s) : Administrateur de projet openMairie.

Par défaut un projet sur readthedocs.org gère uniquement la dernière version de
la documentation 'latest' en récupérant la branche par défaut de la documentation
sur le dépôt du code source ('master' ou 'trunk').

Il est possible de gérer plusieurs versions de la documentation pour obtenir des 
URL du style : 

* http://omframework.readthedocs.org/fr/4.2/
* http://omframework.readthedocs.org/fr/4.3/
* http://omframework.readthedocs.org/fr/4.4/

Chaque version dans readthedocs.org, correspond à une branche sur le dépôt du 
code source du projet.

On conseille de faire une version de documentation par version mineure de logiciel 
(périmètre fonctionnel figé), avec une numérotation à deux nombres X.Y correspondant 
à celle du logiciel X.Y.Z.

On déconseille désormais de maintenir la branche *master* avec pour équivalent la 
version *latest*, on peut donc :

* créer une branche *1.0* et la désigner comme *branche par défaut* 
* activer une version *1.0* sur Read the Docs liée à cette branche *1.0*
* ne pas supprimer la branche *master* inutilisée avant d'avoir éprouvé la compilation de la version *1.0* et de l'avoir vu apparaitre dans la liste du menu ``Admin > Version`` du projet

On conseille de n'utiliser que des branches numérotées. La branche de la 
dernière version sera identifiée dans git comme "branche par défaut".

Pendant la première mise au point d'une version de documentation, on peut ajouter une note
sous le titre dans le fichier ``README.rst`` à la racine du projet, avec le code suivant:

.. code:: rest

   .. note::
       
       Documentation en cours de développpement
       

Il est utile de conserver les branches des anciennes versions de documentation pour les
utilisateurs qui ne souhaitent pas migrer vers une version plus récente. 

Pour la rédaction des fichiers source de la documentation, on pourra consulter avec profit
le paragraphe ':ref:`sphinx`', et notamment la référence *reStructuredText* proposée.

Pour la publication de la documentation ReadtheDocs vers openmairie.org, consulter la rubrique ':ref:`regles_publication`'.
